Git.McGowan.Media
======

This repository contains the code related to my Git-centric subdomain, [Git.McGowan.Media](https://git.mcgowan.media).  This subdomain provides a simple interface which users can access to link to my various Git repos.  Those repositories include:
* [GitHub](https://github.com/matthewjmcgowan)
* [GitLab](https://gitlab.com/matthewjmcgowan)
* [Bitbucket](https://bitbucket.org/matthewjmcgowan/)

If you have any questions, comments, or concerns, please feel free to email me: 

Matt McGowan

[matthew@mcgowan.media](mailto:matthew@mcgowan.media)
